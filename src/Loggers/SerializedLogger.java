package Loggers;

import Program.LoggedStudent;
import Program.Status;
import Program.Student;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Mateusz on 04.05.2017.
 */
public class SerializedLogger implements Logger {
    private static boolean isFileExist;
    private static String PATH = null;
    private final static SimpleDateFormat dateFormat = new SimpleDateFormat("yy_MM_dd_HH_mm_ss_ms");


    private FileOutputStream fileOutputStream;
    private ObjectOutputStream objectOutputStream;

    private FileInputStream fileInputStream;
    private ObjectInputStream objectInputStream;


    public SerializedLogger()
    {
        PATH = "SerializeLogger\\" + dateFormat.format(new Date()).toString() +"_SERIALIZE_LOG.ser";
        isFileExist = false;

        try {
            fileOutputStream = new FileOutputStream(PATH, isFileExist);
            objectOutputStream = new ObjectOutputStream(fileOutputStream);



        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    @Override
    public void log(String status, Student student) {

        Status studentStatus;
        if (status.equals("ADDED"))
            studentStatus = Status.ADDED;
        else
            studentStatus = Status.REMOVED;


        LoggedStudent loggedStudent = new LoggedStudent(student, (System.currentTimeMillis() / 1000L),studentStatus );


        try
        {
            objectOutputStream.writeObject(loggedStudent);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public List<LoggedStudent> listStudents(String PATH2)
    {
        List<LoggedStudent> listStudents = new ArrayList<LoggedStudent>();

        File file = new File(PATH2);
        if(file.exists() && !file.isDirectory())
        {
            try {
                fileInputStream = new FileInputStream(PATH2);
                objectInputStream = new ObjectInputStream(fileInputStream);

                try
                {
                    for (;;)
                    {
                        LoggedStudent loggedStudent = (LoggedStudent) objectInputStream.readObject();
                        listStudents.add(loggedStudent);
                    }
                }catch (EOFException e) {} catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }


            return listStudents;
        }
        else
        {
            return null;
        }


    }
}
