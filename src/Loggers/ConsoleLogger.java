package Loggers;

import Program.Student;

/**
 * Created by Mateusz on 20.03.2017.
 */
public class ConsoleLogger implements Logger {


    @Override
    public void log(String status, Student student) {

        if(student!=null)
        {
            System.out.println(status + ": " + student.getMark() + " " + student.getFirstName() + " " + student.getLastName() + " " + student.getAge());
        }
    }
}
