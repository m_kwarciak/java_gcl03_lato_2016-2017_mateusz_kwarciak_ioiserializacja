package Loggers;

import Program.LoggedStudent;
import Program.Status;
import Program.Student;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mateusz on 04.05.2017.
 */
public class BinaryLogger implements Logger {
   private static boolean isFileExist;
   private static String PATH = "BinaryLogger.txt";
   private DataOutputStream dataOutputStream;
   private DataInputStream dataInputStream;


    @Override
    public void log(String status, Student student) {
        Status studentStatus;
        if (status.equals("ADDED"))
            studentStatus = Status.ADDED;
        else
            studentStatus = Status.REMOVED;

        LoggedStudent loggedStudent = new LoggedStudent(student, (System.currentTimeMillis() / 1000L),studentStatus );

        File file = new File(PATH);
        if (file.exists() && !file.isDirectory())
        {
            isFileExist = true;
        }
        else
        {
            isFileExist = false;
        }

        try
        {
            dataOutputStream = new DataOutputStream(new FileOutputStream(PATH, isFileExist));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        try
        {
            dataOutputStream.writeUTF(loggedStudent.toFile());
        } catch (IOException e) {
            e.printStackTrace();
        }

        try
        {
            if (dataOutputStream !=null)
                dataOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public List<LoggedStudent> listStudents()
    {
        List<LoggedStudent> listStudents = new ArrayList<LoggedStudent>();

        String data = "";
        try {
            dataInputStream = new DataInputStream(new FileInputStream(PATH));
            int length = dataInputStream.available();
            byte[] buf = new byte[length];
            dataInputStream.readFully(buf);



            for(byte b: buf)
            {
                data += (char) b;
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        String[] objectsParts = data.split( "#" );
        for(String el: objectsParts)
        {
            String[] elementParts = el.split(";");
            long time = Long.parseLong(elementParts[0].substring(2));
            Status studentStatus;
            if (elementParts[1].equals("ADDED"))
                studentStatus = Status.ADDED;
            else
                studentStatus = Status.REMOVED;

            Student tmp = new Student();
            tmp.setMark(Double.parseDouble(elementParts[2]));
            tmp.setFirstName(elementParts[3]);
            tmp.setLastName((elementParts[4]));
            tmp.setAge(Integer.parseInt(elementParts[5]));

            LoggedStudent loggedStudent = new LoggedStudent(tmp, time, studentStatus);
            listStudents.add(loggedStudent);
        }

        return listStudents;
    }

}
