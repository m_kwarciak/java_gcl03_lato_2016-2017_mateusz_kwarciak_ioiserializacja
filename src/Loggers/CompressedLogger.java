package Loggers;

import Program.Student;

import java.io.*;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Created by Mateusz on 03.05.2017.
 */
public class CompressedLogger implements Logger, Closeable{
    private final static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd___HH_mm_ss_ms");
    private static Date datePrevious = new Date(0);
    private static Date date;
    private TextLogger textLogger;
    private Map<String, String> zipPropertiesCreate = new HashMap<>();
    private Map<String, String> zipPropertiesAppend = new HashMap<>();
    private Path path;
    private URI uri;
    private static Path nf;

    public CompressedLogger()
    {
        textLogger = new TextLogger();
        zipPropertiesAppend.put("create", "true");
        zipPropertiesCreate.put("append", "true");
        path = Paths.get("archive.zip");
        uri = URI.create("jar:" + path.toUri());

    }


    @Override
    public void log(String status, Student student)
    {
        date = new Date();

        if(date.toString().equals(datePrevious.toString()))
        {
            try (FileSystem fs = FileSystems.newFileSystem(uri, zipPropertiesAppend)) {
                Path nf = fs.getPath(dateFormat.format(date) + ".txt");
                try (Writer writer = Files.newBufferedWriter(nf, StandardCharsets.UTF_8, StandardOpenOption.APPEND)) {
                    writer.write(textLogger.textToZIP(status,student) + System.lineSeparator());
                    //writer.write(System.lineSeparator());
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else
        {
            try (FileSystem fs = FileSystems.newFileSystem(uri, zipPropertiesCreate)) {
                Path nf = fs.getPath(dateFormat.format(date) + ".txt");
                try (Writer writer = Files.newBufferedWriter(nf, StandardCharsets.UTF_8, StandardOpenOption.CREATE)) {
                    writer.write(textLogger.textToZIP(status,student) + System.lineSeparator());
                    //writer.write(System.lineSeparator());
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        datePrevious = new Date();

    }

    @Override
    public void close() throws IOException {
        System.out.println("Close");
    }
}

/*
else {
            try (FileSystem fs = FileSystems.newFileSystem(uri, zipProperties)) {
                Path nf = fs.getPath(dateFormat.format(date) + ".txt");
                try (Writer writer = Files.newBufferedWriter(nf, StandardCharsets.UTF_8, StandardOpenOption.CREATE)) {
                    writer.write(textLogger.textToZIP(status, student));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

 */