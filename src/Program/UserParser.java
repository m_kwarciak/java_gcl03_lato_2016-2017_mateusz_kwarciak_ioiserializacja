package Program;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mateusz on 02.05.2017.
 */
public class UserParser {
    public static List<User> parse(File file ) throws IOException
    {
        try( InputStream stream = new FileInputStream( file ) )
        {
            return parse( stream );
        }
    }


    public static List<User> parse( InputStream stream ) throws IOException
    {
        try( InputStreamReader reader = new InputStreamReader( stream ) )
        {
            return parse( reader );
        }
    }

    public static List<User> parse( InputStreamReader reader ) throws IOException
    {
        List<User> result = new ArrayList<>();

        try( BufferedReader tmp = new BufferedReader( reader ) )
        {
            while( true )
            {
                String line = tmp.readLine();

                if( line == null )
                    break;

                User user = parseUser( line );

                if( user == null )
                    continue;

                result.add( user );
            }

        }

        return result;
    }

    private static User parseUser( String line )
    {
        String[] parts = line.split( ";" );

        if( parts.length == 5 )
        {
            for( String el : parts )
            {
                if( el.isEmpty() )
                    return null;
            }

            try
            {
                User user = new User(parts[0], parts[1], Integer.parseInt(parts[2]), parts[3], parts[4]);

                return user;
            }
            catch ( NumberFormatException e )
            {
                return null;
            }
        }

        return null;
    }
}
