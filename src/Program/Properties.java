package Program;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mateusz on 02.05.2017.
 */
public class Properties {

    private String PATH = new String("usersDATA.txt");


    public List<User> loadUserDATA()
    {
        File file = new File(PATH);
        try {
            List<User> list = UserParser.parse(file);
            return list;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void saveUserDATA(User user) throws FileNotFoundException, UnsupportedEncodingException
    {
        Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(PATH, true),"UTF-8"));
        try {
            writer.write(user.toFIleString());
            writer.write(System.lineSeparator());
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }





}

