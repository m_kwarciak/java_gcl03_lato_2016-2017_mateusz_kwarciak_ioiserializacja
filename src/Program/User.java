package Program;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Created by Mateusz on 09.04.2017.
 */
public class User {
    private String login;
    private String password;
    private Integer age;
    private String address;
    private String sex;

    public User(String login, String password, int age, String address, String sex)
    {
        this.login = login;
        this.password = password;
        this.age = age;
        this.address = address;
        this.sex = sex;
    }


    public String getLogin(){return  login;}
    public String getPassword(){return  password;}
    public Integer getAge(){return  age;}
    public String getAddress(){return  address;}
    public String getSex(){return  sex;}

    public String getStringLoginProperty(){return  login; }
    public String getStringPasswordProperty(){return  password; }

    public void setLogin(String login){
        this.login = login;
    }

    public void setPassword(String password){
        this.password = password;
    }

    public void setAge(Integer age){
        this.age = age;
    }

    public void setAddress(String address){
        this.address = address;
    }

    public void setSex(String sex){
        this.sex = sex;
    }


    public  String toString()
    {
        return login + " " + password;
    }
    public String toFIleString() { return login + ";" + password + ";" + age + ";" + address + ";" + sex;}
}

